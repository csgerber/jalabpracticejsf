/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.uchicago.gerber;

import javax.inject.Named;
import javax.enterprise.context.RequestScoped;

/**
 *
 * @author ag
 */
@Named(value = "customerController")
@RequestScoped
public class CustomerController {

    /**
     * Creates a new instance of CustomerController
     */
    public CustomerController() {
    }
    
    
    public String processMe(){
        
        System.out.println(" do some processing");
        return "confirm.xhtml";
    }
    
}
