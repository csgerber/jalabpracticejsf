/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.uchicago.gerber;

import javax.inject.Named;
import javax.enterprise.context.Dependent;

/**
 *
 * @author ag
 */
@Named(value = "address")
@Dependent //this will inherit the scope of whatever bean it's used inside
public class Address {

    private String line1;
    private String line2;
    private String city;
    private String state;
    private String zip;

    public String getLine1() {
        return line1;
    }

    public void setLine1(String line1) {
        this.line1 = line1;
    }

    public String getLine2() {
        return line2;
    }

    public void setLine2(String line2) {
        this.line2 = line2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }
    
    
    
    /**
     * Creates a new instance of Address
     */
    public Address() {
    }
    
}
